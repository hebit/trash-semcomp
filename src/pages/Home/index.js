import React from "react";
import Container, {
  Main,
  VideoOverlay,
  PageContainer,
  Menu,
  ul,
  ContentContainer,
  Summary
} from "./styles";
import divingImg from "../../assets/diving-man.png";
import logoImg from "../../assets/logo.png";

import "./reset.css";

export default () => (
  <Container>
    <video
      src={"https://a.uguu.se/kJpbFvYrS3PL_bgVideo.mp4"}
      autoPlay={true}
      muted={true}
      loop
    />
    <VideoOverlay />
    <PageContainer>
      <img className="diving-man" src={divingImg} />
      <ContentContainer>
        <Menu className="menu">
          <menu>
            <li>
              <ul>
                <a href="#">Início</a>
              </ul>
              <ul>
                <a href="#">Torne-se um patrocinador</a>
              </ul>
              <ul>
                <a href="#">Entre em contato</a>
              </ul>
              <ul>
                <a href="#">Inscreva-se</a>
              </ul>
            </li>
          </menu>
          {/* <menu>
            <li>
              <ul>
                <a href="#">Início</a>
              </ul>
              <ul>
                <a href="#">Programação</a>
              </ul>
              <ul>
                <a href="#">Palestrantes</a>
              </ul>
              <ul>
                <a href="#">Maratona</a>
              </ul>
              <ul>
                <a href="#">Como Chegar</a>
              </ul>
            </li>
          </menu> */}
        </Menu>
        <Main>
          <Summary className="summary">
            <img src={logoImg} className="logo" />
            <p>18 a 21 de março de 2019</p>
          </Summary>
          <section className="about">
            <p className="intro">
              A SEMCOMP UFBA (Semana de Computação da UFBA) é um evento que visa
              fomentar o empreendedorismo e movimentar a comunidade de
              computação do estado da Bahia.
            </p>
            <p>
              Em sua sétima edição, a SEMCOMP está com uma programação bem
              diversificada.
            </p>
          </section>
          <section className="section">
            <h1>Gostaria de Apoiar-nos?</h1>
            <p>Veja nosso edital patrocínio</p>
          </section>
          <section className="section">
            <h1>Por que Oceano?</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
              vitae facilisis leo, sed finibus elit. Suspendisse potenti.
              Maecenas gravida magna sed turpis maximus vulputate. Vivamus
              hendrerit, mauris congue facilisis viverra, leo ipsum sodales
              magna, id placerat risus dolor ut dolor. Praesent felis nisi,
              viverra nec lorem eu, feugiat finibus libero. Aenean sem est,
              placerat auctor lobortis ac, vulputate nec felis. Morbi justo
              magna, mollis feugiat ullamcorper id, vulputate ac turpis. In
              molestie accumsan odio, ac tempor lacus. Proin maximus odio vitae
              eros condimentum, eu facilisis massa lobortis. Donec erat tortor,
              sagittis sed mauris in, ultricies imperdiet enim. Nullam lacinia
              at mauris sit amet pharetra. Aliquam malesuada tincidunt purus nec
              fermentum. Integer ac ornare libero. Cras in lacinia odio. Nunc
              tempor augue quis dui efficitur fringilla.{" "}
            </p>
          </section>
        </Main>
      </ContentContainer>
    </PageContainer>
  </Container>
);
