import styled from "styled-components";
import bgVidePreview from "../../assets/bgvideo-preview.png";

export default styled.div`
  /* // background: linear-gradient(
  //     186.29deg,
  //     rgba(1, 15, 24, 0.42) 4.99%,
  //     #000000 96.72vh
  //   ),
  //   #010b0f; */

    background: linear-gradient(to bottom, #010108, #000);
  color: #fafafa;
  margin: 0;
  padding: 0;
  width: 100vw;
  overflow: hidden;
  min-height: 100vh;
  position: relative;
  margin: 0 auto;

  video {
    height: auto;
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 0;
    min-width: 100vw;
    background: url("${() => bgVidePreview}");
  }
`;

export const PageContainer = styled.div`
  color: #fafafa;
  /* display: grid;
  grid-template-columns: 1fr minmax(450px, 1fr) minmax(450px, 1fr) 1fr; */
  margin: 0;
  padding: 0;
  width: 100%;
  max-width: 1500px;
  position: relative;
  overflow-x: visible;
  margin: 0 auto;
  /* min-height: 100vh; */
  min-height: 1740px;

  .diving-man {
    position: absolute;
    width: 1300px;
    top: -200px;
    z-index: 200;
    right: -340px;
    transition: all 1s ease-in-out;

    @media screen and (max-width: 750px) {
      opacity: 0.6;
    }

    @media screen and (max-width: 700px) {
      opacity: 0.4;
    }

    @media screen and (max-width: 670px) {
      top: -120px;
      opacity: 0.3;
    }
  }
`;

export const ContentContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  min-height: 1740px;
  display: grid;
  grid-template-columns: 1fr 940px 1fr;
  @media screen and (max-width: 940px) {
    grid-template-columns: 0 1fr 0;
  }
  align-content: flex-start;
  /* grid-template-rows: 60px auto-f; */
  overflow: hidden;
  z-index: 300;
`;

export const Main = styled.main`
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr 1fr;
  color: #fafafa;
  box-sizing: border-box;
  padding: 0 20px;
  @media screen and (max-width: 950px) {
    padding: 0 34px;
  }
  .about {
    margin-top: 140px;
    @media screen and (max-width: 670px) {
      grid-column: 1/3;
    }
  }

  .section {
    margin-top: 80px;
    grid-column: 1/3;
  }
`;

export const VideoOverlay = styled.div`
  background: linear-gradient(
    183.6deg,
    rgba(0, 0, 0, 0) -65.61%,
    #010102 86.22%
  );
  width: 100vw;
  min-width: 664px;
  /* min-height: 334px; */
  padding-bottom: 56.25%;
  z-index: 100;
  height: 0;
  position: absolute;

  @media screen and (max-width: 664px) {
    min-height: 374px;
    padding: 0;
  }
`;

export const Menu = styled.nav`
  display: flex;
  align-items: center;
  grid-column: 2/3;
  height: 80px;
  font-size: 18px;
  @media screen and (max-width: 950px) {
    display: none;
  }
  menu {
    padding: 0 12px 0;
    margin: 0;
    flex-grow: 1;
  }
  li {
    list-style: none;
    display: flex;
    justify-content: space-between;
    padding: 0;
    margin: 0;
  }
  ul {
    margin: 0;
    padding: 0;
    :first-child {
      a {
        text-decoration: none;
      }
    }
  }
  a {
    color: #ddd;
    text-decoration: none;

    /* text-decoration: line-through; */

    :hover {
      text-decoration: underline;
      /* text-decoration: line-through; */
    }
  }
`;

export const Summary = styled.div`
  grid-column: 1/3;
  margin-top: 140px;
  box-sizing: border-box;
  /* padding: 0 20px; */
  width: 50%;
  display: flex;
  flex-direction: column;
  transition: width 1s ease-in-out;

  align-items: flex-start;
  @media screen and (max-width: 670px) {
    width: 100%;
    align-items: center;
  }
  @media screen and (max-width: 950px) {
    /* align-items: center; */
  }

  img.logo {
    width: 305px;
    min-width: 307px;
    @media screen and (max-width: 670px) {
      width: 70%;
      margin-left: 0;
    }
  }
  p {
    text-align: center;
    margin-top: 4px;
  }
`;
